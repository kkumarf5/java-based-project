FROM java:8-jdk
COPY ./build/libs/*.jar /c/users/kaukumar/downloads/myjar.jar
WORKDIR /c/users/kaukumar/downloads
EXPOSE 8080
CMD ["java","-jar","/c/users/kaukumar/downloads/myjar.jar"]